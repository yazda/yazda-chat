#!/usr/bin/env node

/**
 * Module dependencies.
 */
if(process.env.NODE_ENV === 'production') {
  require('newrelic');
}

var mongoose = require('mongoose');
var uri = process.env.MONGO_URL || 'mongodb://localhost/yazda-chat';
var db = mongoose.connect(uri).connection;
var health = require('health-route');
var url     = require('url');

process.on('SIGINT', function() {
  db.close(function() {
    process.exit(0);
  });
});

db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function() {
  var debug = require('debug')('redispubsub');
  var http = require('http');

  /**
   * Get port from environment and store in Express.
   */
  var port = normalizePort(process.env.PORT || '8080');

  /**
   * Create HTTP server.
   */
  var server = http.createServer(function(req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Request-Method', '*');
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
    res.setHeader('Access-Control-Allow-Headers', '*');

    var method = req.method.toLowerCase()
    var path = (url.parse(req.url)).pathname

    if((path === '/health' || path === '/') && method === 'get') {
      return health(req, res)
    } else {
      res.writeHead(404, 'Not Found');
      return res.end('Not Found\n')
    }
  });

  /*
   * enable node cluster and sticky-session
   */
  var sticky = require('sticky-session');
  sticky.listen(server, port);

  /*
   * setup socket.io, and socket-session
   */
  var socketIO = require('socket.io');
  var io = socketIO(server);
  io.set('origins', '*:*');
  io.set('transports', [
      'websocket',
      'flashsocket',
      'htmlfile',
      'xhr-polling',
      'jsonp-polling'
  ]);

  var setEvents = require('./events');
  setEvents(io);

  /**
   * Normalize a port into a number, string, or false.
   */

  function normalizePort(val) {
    var port = parseInt(val, 10);

    if(isNaN(port)) {
      // named pipe
      return val;
    }

    if(port >= 0) {
      // port number
      return port;
    }

    return false;
  }
});
