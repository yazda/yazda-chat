'use strict';
var uri = process.env.BASE_URL || 'http://dev.yazdaapp.com';
var redisUri = process.env.REDIS_URL;

var redis = require('redis');
var pub = redis.createClient(redisUri);
var request = require('request');
var _ = require('lodash');
var adventureChat = require('../models/adventureChat.js');
var chat = require('../models/chat.js');

module.exports = function(io) {
  require('socketio-auth')(io, {
    timeout:          10000,
    authenticate:     function(socket, data, callback) {
      //get credentials sent by the client
      var token = data.token;

      request({
            headers: {
              'Authorization': 'Bearer ' + token
            },
            uri:     uri + '/v1/account/me',
            method:  'GET'
          },
          function(err, res, body) {
            if(err) {
              console.log(err);
              callback(new Error("User not found"));
            } else {
              socket.token = data.token;
              socket.user = JSON.parse(body).user;
              callback(null, socket.user);
            }
          });
    },
    postAuthenticate: function(socket, data) {
      var direct_chat = redis.createClient(redisUri);
      direct_chat.subscribe('direct_chat');

      // token and retrieve their info and store so we can save data later
      socket.sub = redis.createClient({
        disable_resubscribing: true,
        url:                   redisUri
      });

      setInterval(function() {
        socket.emit('ping', {beat: 1});
      }, 1000);

      /*
       Use Redis' 'sub' (subscriber) client to listen to any message from Redis to server.
       When a message arrives, send it back to browser using socket.io
       */
      socket.sub.on('message',
          function(channel, message) {
            var obj = JSON.parse(message);

            console.log('received ' + channel);
            socket.emit(channel, obj);
          }
      );

      socket.on('unreadAdventures', function(msg) {
        console.log(msg);
        adventureChat().unreadAdventureChats(msg.adventureIds, socket.user.id, function(err, doc) {
          if(err) {
            console.log(err);
          } else {
            console.log(doc);
            socket.emit('unreadAdventures', doc);
          }
        });
      });

      /*
       When the user sends a chat message, publish it to everyone (including myself) using
       Redis' 'pub' client we created earlier.
       Notice that we are getting user's name from session.
       */
      socket.on('chat', function(msg) {
        if(msg.adventureId) {
          request({
                headers: {
                  'Authorization': 'Bearer ' + socket.token
                },
                uri:     uri + '/v1/adventures/' + msg.adventureId,
                method:  'GET'
              },
              function(err, res, body) {
                if(!err) {
                  var channelName = 'adventures:' + msg.adventureId;
                  var reply = {
                    action:      'message',
                    msg:         msg.msg,
                    adventureId: msg.adventureId,
                    user:        socket.user,
                    sent_at:     new Date(),
                    read:        [socket.user.id]
                  };

                  adventureChat().createAdventureMessage(reply, function(err) {
                    if(!err) {
                      pub.publish(channelName, JSON.stringify(reply));
                    } else {
                      console.log('Error getting adventure: ' + err);
                    }
                  });
                } else {
                  console.log('Cant find adventure: ' + err);
                }
              });

        } else {
          if(!msg.sentTo) {
            msg.sentTo = [];
          }

          var reply = {
            action:         'message',
            msg:            msg.msg,
            user:           socket.user,
            conversationId: msg.conversationId,
            sentTo:         msg.sentTo,
            sent_at:        new Date()
          };

          if(msg.conversationId) {
            chat().userIdsByConversationId(msg.conversationId, function(err, doc) {
              if(!err) {
                var ids = _.map(doc.sentTo, 'id');

                request({
                      headers: {
                        'Authorization': 'Bearer ' + socket.token
                      },
                      uri:     uri + '/v1/users?ids=' + ids.join(','),
                      method:  'GET'
                    },
                    function(err, res, body) {
                      if(!err) {
                        var users = JSON.parse(body);

                        reply.sentTo = users.users;

                        chat().createMessage(reply, function(err, resp) {
                          if(!err) {
                            pub.publish('direct_chat', JSON.stringify(resp));
                          } else {
                            console.log(err);
                          }
                        });
                      } else {
                        console.log('Error getting users: ' + err);
                      }
                    });
              } else {
                console.log('Cant find users: ' + err);
              }
            });
          } else {
            reply.sentTo.push(socket.user);
            chat().createMessage(reply, function(err, resp) {
              if(!err) {
                pub.publish('direct_chat', JSON.stringify(resp));
              } else {
                console.log(err);
              }
            });
          }
        }
      });

      /*
       When a user joins the channel, publish it to everyone (including myself) using
       Redis' 'pub' client we created earlier.
       Notice that we are getting user's name from session.
       */
      socket.on('join', function(join) {
        var channelName = 'adventures:' + join.adventureId;

        adventureChat().findLatestMessages(join.adventureId, null, null, function(err, chats) {
          socket.emit(channelName + ':paged', chats);
        });

        socket.sub.subscribe(channelName);
      });

      socket.on('conversations', function(conv) {
        if(conv.conversationId) {
          if(conv.action === 'list') {
            chat().findLatestMessages(conv.conversationId, null, null, function(err, data) {
              socket.emit('conversations:add', data);
            });
          } else if(conv.action === 'mark_read') {
            if(!socket.user) {
              setTimeout(function() {
                chat().markAsRead(conv.conversationId, socket.user.id);
              }, 1000);
            } else {
              chat().markAsRead(conv.conversationId, socket.user.id);
            }
          }
        } else if(conv.adventureId) {
          if(conv.action === 'mark_read') {
            if(!socket.user) {
              setTimeout(function() {
                adventureChat().markAsRead(conv.adventureId, socket.user.id);
              }, 1000);
            } else {
              adventureChat().markAsRead(conv.adventureId, socket.user.id);
            }
          }
        }
      });

      socket.on('disconnect', function() {
        socket.sub.unsubscribe();
        direct_chat.unsubscribe();
        console.log('disconnected');
      });

      /*
       When a user unsubscribes, unsubscribe them from redis
       */
      socket.on('leave', function(obj) {
        var join = obj;
        var channelName = 'adventures:' + join.adventureId;

        // TODO unsubscribe instead of killing client
        socket.sub.unsubscribe(channelName);

        console.info('left channel :' + channelName);
      });

      chat().findConversations(socket.user.id, function(err, msgs) {
        socket.emit('conversations', {action: 'conversations', messages: msgs});

        direct_chat.on('message',
            function(channel, message) {
              var obj = JSON.parse(message);

              console.log('received ' + channel);

              if(_.find(obj.sentTo, {id: socket.user.id})) {
                obj.unread = 1;
                socket.emit(channel, obj);
              }
            }
        );
      });
    }
  });
};
