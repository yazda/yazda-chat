var uuid = require('node-uuid');
var mongoose = require('mongoose');
var request = require('request');
var _ = require('lodash');

mongoose.set('debug', true);

var Chat;

var chatSchema = mongoose.Schema({
      conversationId: {type: String, required: true},
      msg:            {type: String, required: true},
      sent_at:        {type: Date, required: true},
      user:           {
        id:                {type: Number, required: true},
        name:              {type: String, required: true},
        profile_image_url: {type: String, required: true},
        profile_image_thumb_url: {type: String, required: true}
      },
      sentTo:         [
        {
          id:                {type: Number, required: true},
          name:              {type: String, required: true},
          profile_image_url: {type: String, required: true},
          profile_image_thumb_url: {type: String, required: true},
          read:              {type: Boolean, default: false}
        }
      ]
    },
    {
      timestamps:      {createdAt: 'created_at'},
      collection:      'chats',
      strict:          true,
      useNestedStrict: true
    });

// TODO careful with indexing in production
//chatSchema.index({'sentTo.id': 1, 'sentTo.read': 1, sent_at: 1});
//chatSchema.index({sent_at: 1, conversationId: 1});

//TODO do we want better finders for adventure chat schema?
Chat = mongoose.model('Chat', chatSchema);

module.exports = function() {
  function saveMessage(msg, errCallback) {
    var message = Chat(msg);

    message.save(function(err, val) {
      errCallback(err, val);
    });
  }

  return {
    userIdsByConversationId: function(conversationId, callback) {
      Chat.findOne({conversationId: conversationId})
          .exec(callback);
    },
    createMessage:           function(msg, errCallback) {
      if(msg.conversationId) {
        // TODO validate user has access to this conversation
        saveMessage(msg, errCallback);
      } else {
        var ids = _.map(msg.sentTo, 'id');

        Chat.findOne({
              'sentTo.id': {$all: ids},
              sentTo:      {$size: ids.length}
            },
            'conversationId',
            {
              lean: true
            },
            function(err, doc) {
              if(doc) {
                msg.conversationId = doc.conversationId;
              } else {
                msg.conversationId = uuid.v4();
              }

              saveMessage(msg, errCallback);
            });
      }
    },
    findConversations:       function(userId, callback) {
      Chat.distinct('conversationId',
          {'sentTo.id': userId},
          function(err, conversationIds) {
            Chat
                .aggregate([
                  {
                    $match: {
                      conversationId: {
                        $in: conversationIds
                      }
                    }
                  },
                  {
                    $sort: {sent_at: -1}
                  },
                  {
                    $project: {
                      conversationId: 1,
                      sentTo:         1,
                      created_at:     1,
                      sent_at:        1,
                      msg:            1,
                      user:           1,
                      isRead:         {
                        $size: {
                         $filter: {
                           input: '$sentTo',
                           as:    'sent',
                           cond:  {
                             $and: [{$eq: ['$$sent.id', userId]},
                               {$ne: ['$$sent.read', true]}]
                           }
                         }
                        }
                      }
                    }
                  },
                  {
                    $group: {
                      _id:            '$conversationId',
                      id:{$first:'$_id'},
                      conversationId: {$first: '$conversationId'},
                      sentTo:         {$first: '$sentTo'},
                      created_at:     {$first: '$created_at'},
                      sent_at:        {$first: '$sent_at'},
                      msg:            {$first: '$msg'},
                      user:           {$first: '$user'},
                      unread:         {$sum: '$isRead'}
                    }
                  }
                ])
                .exec(callback);
          });
    },
    findLatestMessages:      function(conversationId, _id, pageSize, callback) {
      var query = {conversationId: conversationId};

      if(!pageSize) {
        pageSize = 100;
      }

      if(_id) {
        query[_id] = {$gt: _id};
      }

      Chat
          .find(query)
          .sort({sent_at: 1})
          .limit(pageSize || 100)
          .exec(callback);
    },
    markAsRead:              function(conversationId, userId) {
      Chat
          .update(
              {
                $and: [
                  {
                    conversationId: conversationId
                  },
                  {
                    sentTo: {
                      $elemMatch: {
                        id:   userId,
                        read: {$ne: true}
                      }
                    }
                  }
                ]
              },
              {'sentTo.$.read': true},
              {multi: true})
          .exec(function() {
          });
    }
  }
};
//
//db.chats.aggregate(
//    {
//      $sort: {sent_at: 1}
//    },
//    {
//      $project: {
//        'sentTo': 1,
//        isRead:   {
//$size: {
//          $cond: {
//            if:               {
//              $and: [{$eq: ['$sentTo.id', 1]}, {$eq: ['$sentTo.read', true]}]
//            }, then: 0, else: 1
//          }
//}
//        }
//      }
//    })
