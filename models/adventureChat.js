var mongoose = require('mongoose');

var AdventureChat;

var adventureChatSchema = mongoose.Schema({
      msg:         {type: String, required: true},
      adventureId: {type: Number, required: true},
      sent_at:     {type: Date, required: true},
      // TODO careful with indexing in production
      user:        {
        id:                      {type: Number, required: true},
        name:                    {type: String, required: true},
        profile_image_url:       {type: String, required: true},
        profile_image_thumb_url: {type: String, required: true}
      },
      read:        [Number]
    },
    {
      timestamps:      {createdAt: 'created_at'},
      collection:      'chats',
      strict:          true,
      useNestedStrict: true
    });

//adventureChatSchema.index({adventureId: 1, sent_at: 1});

//TODO do we want better finders for adventure chat schema?
AdventureChat = mongoose.model('AdventureChat', adventureChatSchema);

module.exports = function() {
  return {
    unreadAdventureChats:   function(adventureIds, userId, callback) {
      AdventureChat
          .aggregate([
            {
              $match: {
                adventureId: {
                  $in: adventureIds
                },
                read:        {$nin: [userId]}
              }
            },
            {
              $group: {
                _id:         '$adventureId',
                unread:      {$sum: 1}
              }
            }
          ])
          .exec(callback);
    },
    createAdventureMessage: function(msg, callback) {
      var message = AdventureChat(msg);

      message.save(function(err) {
        callback(err);
      })
    },
    findLatestMessages:     function(adventureId, _id, pageSize, callback) {
      var query = {adventureId: adventureId};

      if(_id) {
        query[_id] = {$gt: _id};
      }

      AdventureChat
          .find(query)
          .sort({sent_at: 1})
          .limit(pageSize || 100)
          .exec(callback);
    },
    markAsRead:             function(adventureId, userId) {
      AdventureChat
          .update(
              {
                $and: [
                  {
                    read: {$nin: [userId]}
                  },
                  {
                    adventureId: adventureId
                  }
                ]
              },
              {$addToSet: {read: userId}},
              {multi: true})
          .exec(function() {
          });
    }
  }
};
