FROM node:6.10
MAINTAINER james.stoup@yazdaapp.com

RUN apt-get update && apt-get install -y \
    build-essential \
    nodejs \
    imagemagick \
    libpq-dev \
    libjpeg62

RUN mkdir -p /app
WORKDIR /app

COPY package.json ./
RUN npm install

COPY . ./
